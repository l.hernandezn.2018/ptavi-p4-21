#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import time
import json

# Constantes. Puerto.
port = 6002


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """Echo server class"""
    usuarios = {}

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        # Mira si ha caducado el registro de un usuario (no necessario, pero me ayudo a escribir el resto)
        borrar = []
        for usuario, (server, expires) in self.usuarios.items():
            if expires > 0 and time.time() >= expires:
                borrar.append(usuario)
        for usuario in borrar:
            del self.usuarios[usuario]
        # Si el usuario envia el mensaje correctamente, hay que registrar o dar de baja dependiendo del mensaje
        msg = self.rfile.read(1024)
        if msg.startswith(b"REGISTER sip:"):
            expires = -1
            msg = msg.replace(b"REGISTER sip:", b"")
            # Todos los mensajes deberian tener eso al final
            if b" SIP/2.0\r\n" in msg:
                msg = msg.replace(b" SIP/2.0\r\n", b"")
            # Registros que se pueden caducar tienen 'Expires' en en mensaje
            if b"Expires: " in msg:
                # intenta sacar en numero de segundos antes de que se caduca el registro
                try:
                    # msg.split(":") -> [..., "Expires", " 0\r\n\r\n"]
                    expires = int(msg.split(b":")[-1].replace(b" ", b"").replace(b"\r\n\r\n"))
                # si numero es invalido, emite un error
                except Exception as e:
                    print("Error", e)
                    self.wfile.write("SIP/2.0 404 ERROR \r\n\r\n".encode('utf-8'))
                    return
                # cojemos la parte del mensaje que se termina donde empieza la palabra 'Expires'
                msg = msg[:msg.find(b"Expires")]
            # Los demas se terminan con una seguda nueva linea
            else:
                msg = msg.replace(b"\r\n", b"")

            usuario = msg.decode('utf-8')

            # Si es menos que 0, el registro no se puede caducar
            if int(expires) < 0:
                self.usuarios[usuario] = (self.client_address[0], 0.0)
            # si es mas que 0, el registro se acaba despues de 'expires' segundos
            elif int(expires) > 0:
                self.usuarios[usuario] = (self.client_address[0], time.time() + expires)
            # si es 0, el usuario se esta danda de baja
            else:
                # intenta quitar el usuario del dicionario
                try:
                    del self.usuarios[usuario]
                # si el usuario no existe, se emite un error
                except KeyError:
                    print("Error no user: ", usuario)
                    self.wfile.write("SIP/2.0 404 ERROR \r\n\r\n".encode('utf-8'))
                    return

            self.wfile.write("SIP/2.0 200 OK \r\n\r\n".encode('utf-8'))
            self.registered2json()
        else:
            self.wfile.write("SIP/2.0 404 ERROR \r\n\r\n".encode('utf-8'))

    def registered2json(self):

        with open('registered.json', 'w') as fichero:
            json.dump(self.usuarios, fichero)

    def json2registered(self):
        try:
            with open('registered.json', 'r') as fichero:
                self.usuarios = json.load(fichero)
        except FileNotFoundError:
            sys.exit("Error con el archivo")


def main():
    try:
        serv = socketserver.UDPServer(('', port), SIPRegisterHandler)
        print(f"Server listening in port ({port})...")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
