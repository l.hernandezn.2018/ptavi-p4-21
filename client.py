#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""
import sys
import socket

server = sys.argv[1]
port = int(sys.argv[2])
metodo = sys.argv[3]
usuario = sys.argv[4]
expires = sys.argv[5]


def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((server, port))
            print(f"Registrando a {server}: {port}:", usuario)
            msg = b"REGISTER sip:" + usuario.encode('utf-8') + b' SIP/2.0\r\n'
            if int(expires) >= 0:
                msg += b"Expires: " + str(expires).encode('utf-8')
                msg += b"\r\n\r\n"
            else:
                msg += b"\r\n"
            my_socket.send(msg)
            data = my_socket.recv(1024)
            print('Recibido: ', data.decode('utf-8'))
        print("Cliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
